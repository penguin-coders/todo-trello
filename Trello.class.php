<?php

class Trello {


	public $api_token = false;
	public $api_key = false;

	public $organization = false;
	public $list_id = false;
	public $board_id = false;


	// Return a JSON-encoded list of boards relating to the organisation
	public function get_org_boards() {
		if ($this->organization==false) {
			return "Error: organization was not set.";
		}
		$ch = curl_init();
		$url= "https://api.trello.com/1/organizations/".$this->organization."/boards?filter=all&fields=all".
			"&key=".$this->api_key."&token=".$this->api_token;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		return curl_exec($ch);
	}


	// Return a JSON-encoded list of boards relating to the organisation
	public function get_member_boards() {
		if ($this->member==false) {
			return "Error: member was not set.";
		}
		$ch = curl_init();
		$url= "https://api.trello.com/1/member/".$this->member."/boards?filter=all&fields=all".
			"&key=".$this->api_key."&token=".$this->api_token;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		return curl_exec($ch);
	}


	// Return a JSON-encoded list of lists within the board
	public function get_board_lists() {
		if ($this->board_id==false) {
			return "Error: board_id was not set.";
		}
		$ch = curl_init();
		$url= "https://api.trello.com/1/boards/".$this->board_id."/lists?filter=all&fields=all".
			"&key=".$this->api_key."&token=".$this->api_token;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		return curl_exec($ch);
	}


	public function set_board_with_name($name) {
		if ($this->organization==false && $this->member==false) {
			return "Error: member or organization must be set.";
		}

		$member_boards = json_decode($this->get_member_boards());
		$found_board = false;
		foreach($member_boards as $board) {
			if ($board->name==$name) {
				$found_board = $board;
			}
		}

		if ($found_board==false) {
			$org_boards = json_decode($this->get_org_boards());
			$found_board = false;
			foreach($org_boards as $board) {
				if ($board->name==$name) {
					$found_board = $board;
				}
			}
		}

		if ($found_board==false) {
			return "Board with name '$name' not found in organization ".$this->organization;
		} else {
			$this->board_id = $found_board->id;
			return $found_board;
		}
	}


	public function set_list_with_name($name) {
		if ($this->board_id==false) {
			return "Error: board_id must be set.";
		}

		$board_lists = json_decode($this->get_board_lists());
		$found_list = false;
		foreach($board_lists as $list) {
			echo "<pre>".print_r($list,true)."</pre><br><br>";
			if ($list->name==$name) {
				$found_list = $list;
			}
		}

		if ($found_list==false) {
			return "List with name '$name' not found in organization ".$this->organization;
		} else {
			$this->list_id = $found_list->id;
			return $found_list;
		}
	}


	// Find a list on the currently set board
	public function find_list() {
		// https://api.trello.com/1/boards/id/lists?cards=none&card_fields=all&filter=open&fields=all
	}


	// Create a new card on a Trello board
	public function create_card($name, $description) {
		$ch = curl_init();
		$url = "https://api.trello.com/1/cards?name=".urlencode($name)."&desc=".urlencode($description).
			"&pos=bottom&idList=".$this->list_id."&keepFromSource=all&key=".$this->api_key."&token=".$this->api_token;
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$output = curl_exec($ch);
	}

}
