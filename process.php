<?php

error_reporting(E_ALL);
ini_set('display_errors', true);


require_once('Trello.class.php');


if (php_sapi_name()==="cli") {
	define("NEW_LINE","\n");
} else {
	define("NEW_LINE","<br>");
}


$config = require(__DIR__.'/config.php');


if ($config['trello_organization']=="" && $config['trello_member']=="") {
	echo "Error: Trello organization or member must be set";
	exit;
}
if ($config['trello_list']=="") {
	echo "Error: Trello list name must be set";
	exit;
}
if ($config['trello_board']=="") {
	echo "Error: Trello list name must be set";
	exit;
}
if ($config['trello_key']=="") {
	echo "Error: Trello key must be set";
	exit;
}
if ($config['trello_token']=="") {
	echo "Error: Trello token must be set";
	exit;
}


$todos = array();


/**
 * Run this function on start_dir, which will then:
 * - Scan the directory for all contents
 * - If a file is a directory, re-call scan() with the directory as a parameter
 * - If it is a file, scan the contents for comments
 * - Scan the comments found for TODOs, and add matching comments to the $todos array under their file path
 */
function scan($dir) {
	global $config;
	global $todos;
	echo "Processing $dir".NEW_LINE;
	$contents = scandir($dir);
	foreach ($contents as $file) {
		if (in_array($file,$config['ignore'])===true) continue;
		echo "Processing $dir/$file".NEW_LINE;
		if (is_dir("$dir/$file")) {
			echo "Is a directory".NEW_LINE;
			scan("$dir/$file");
		} else {
			echo "Is a file".NEW_LINE;
			$content = file_get_contents("$dir/$file");

			preg_match_all('/(\/\*)(?s:.*?)(\*\/)/',$content,$block_comments, PREG_OFFSET_CAPTURE);
			preg_match_all('/((#|\/\/).*\n)+/',$content,$comments, PREG_OFFSET_CAPTURE);
			preg_match_all('/(<\!\-\-)(?s:.*?)(\-\->)/',$content,$html_comments, PREG_OFFSET_CAPTURE);

			$all_comments = array_merge($block_comments[0], $comments[0], $html_comments[0]);

			// Check all comments for TODOs
			foreach($all_comments as $comment) {
				if ($config['greedy']===true) {
					preg_match('/[^A-Za-z][Tt][Oo](\s|-)?[Dd][Oo][^A-Za-z]/', $comment[0], $comment_todos);
				} else {
					preg_match('/((\(|-)(\s)*|^([\t\s]*)(#|\/\/|\/\*|\*)?([\t\s]*))[^A-Za-z][Tt][Oo](\s|-)?[Dd][Oo][^A-Za-z]/', $comment[0], $comment_todos);
				}

				if ($comment[1]==0) {
					$line_number = 1;
				} else {
					list($before) = str_split($content, $comment[1]); // fetches all the text before the match
					$line_number = strlen($before) - strlen(str_replace("\n", "", $before)) + 1;
				}

				if (count($comment_todos)>0) {
					// TODOs found
					$todos["$dir/$file"][] = array('comment'=>$comment[0], 'line'=>$line_number, 'file'=>"$dir/$file");
				}
			}
		}
	}
}
scan($config['start_dir']);


/**
 * Once the $todos array has been populated by the scan() function:
 * - Loop through each file path in $todos
 * - For each file, create a new trello card at the configured location with
 *      a list of comments in the description
 */
foreach($todos as $file => $todo) {
	$title = count($todo)." TODOs found in ".$file;
	$description = "";
	foreach($todos[$file] as $comment) {
		$description.= "## Comment starting on line ".$comment['line']." ##\n";
		$description.= "```\n";
		$description.= $comment['comment']."\n";
		$description.= "```\n\n";
	}

	echo $title.NEW_LINE;
	echo $description.NEW_LINE;

	$trello = new Trello();
	$trello->api_key = $config['trello_key'];
	$trello->api_token = $config['trello_token'];

	if ($config["trello_member"]=="") {
		$trello->organization = $config["trello_organization"];
	} else {
		$trello->member = $config["trello_member"];
	}
	print_r($trello->set_board_with_name($config['trello_board']));
	echo NEW_LINE.NEW_LINE;
	print_r($trello->set_list_with_name($config['trello_list']));

	echo "list_id: ".$trello->list_id;

	$trello->create_card($title, $description);
}

