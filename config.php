<?php

return [
	/* The directory in which the scan will start */
	'start_dir' => '/path/to/code',

	/* Any file (or directory) names to ignore */
	'ignore' => ['.','..','.git','lib','libs','libraries','resource','resources','vendor'],

	/**
	 * By default, this tries to ignore 'to do's found in the middle of a sentence unless an
	 * opening bracket or hyphen is detected before it begins. Set greedy to true to override this.
	 */
	'greedy' => false,

	/* The API key and token used for Trello integration: trello.com/app-key */
	'trello_key' => '',
	'trello_token' => '',

	/* Only one member OR organization name may be selected */
	'trello_member' => '',
	'trello_organization' => '',

	/* The name of the board within which TODOs will be entered */
	'trello_board' => '',

	/* The name of the list to enter located TODOs into */
	'trello_list' => ''
];
